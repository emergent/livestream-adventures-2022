// playing with patterns

(PathName(thisProcess.nowExecutingPath).parentPath ++ "setup.scd").load;
s.plotTree;

// soundcheck
{PinkNoise.ar(0.2!2)*Env.perc(0.01,1.5).kr(2)}.play(target:~src, outbus:~bus[\reverb]);

// the array class
~array1 = []
~array1 = ~array1.add(3)
~array1

// geometric array: multiplication
~array2 = Array.geom(8, 2, 2);
~array3 = Array.geom(8, 4, 1/2);

// series: addition
~array4 = Array.series(8, 2, 2);
~array5 = Array.series(8, 4, -2);

// fibonacci array
~array6 = Array.fib(12, 5, 4);

// interpolation
~array7 = Array.interpolation(8, 0, 8)

Array.rand(8, 0.0, 12.0)

// fun methods with arrays
~array2.normalizeSum;
~array4.reverse;
~array4.mirror2;
~array4.dupEach(2);
~array4.rotate(2) // rotate clockwise
~array4.rotate(-2) // rotate counterclockwise

~array8 = [~array2, ~array4]
~array8.lace(8)

(
SynthDef(\plong, {
	arg dry = 1;
	var sig, env;
	
	env = Env.perc(\atk.kr(0.01), \rel.kr(1)).kr(2);

	sig = LFTri.ar(
		\freq.kr(330),
		mul: 0.4
	);

	sig = sig * env * \amp.kr(-6.dbamp);

	//sig = Splay.ar(sig);
	sig = Pan2.ar(sig, 0);

	Out.ar(\out.kr(0), sig * dry);
	Out.ar(\fx.kr(0), sig * (1- dry));
}).add;
)
Synth(\plong)
// deterministic patterns
(
Pbindef(\plong, 
	\instrument, \plong,
	\dur, Pseries(0.5, -0.005, 73), // don't let this go negative!
	\dry, 0.66,
	\atk, Pgeom(0.25,0.95,inf), 
	\rel, 0.8,
	\freq, Pseq((24..96).midicps, inf),
	//\amp, 0.5,
	\amp, Pkey(\freq).lincurve(24, 96, 0.8, 0.1, -2)*1.1,
	\out, ~out,
	\fx, ~bus[\reverb],
	\group, ~src,
	\legato, 0.2
);
)
Pbindef(\plong).play(t);
Pbindef(\plong).set(\fadeTime, 2);
Pbindef(\plong).stop;

s.quit;
